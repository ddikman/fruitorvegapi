# Fruit or Vegetable API
This API aims to answer the long standing question of "is it a fruit or a vegetable?".

Most people agree, only then, there's the scientists as well. God bless them.

So, scientifically speaking, the way certain "vegetables" such as tomatos, cucumber and avocado are composed, they are in fact fruits and not vegetables. However, this does not mean we ought to put them in a fruit salad. So, to help guide you into which edibles goes with what here's this API, enjoy!

# Running
First up, run the api by `dotnet run --project fruitorvegapi` then simply visit `http://localhost:5000/` for the swagger api docs.

# Testing
From root folder run `dotnet test fruitorvegapi.tests`.

# Sources for the seeming nonsense
What is a berry (and not an aggregate fruit):
https://en.wikipedia.org/wiki/Berry#Botanical_definition

What's a fruit (and not a vegetable)?
https://simple.wikipedia.org/wiki/List_of_fruits

Common vegetables, just like you know them:
https://simple.wikipedia.org/wiki/Vegetable

# Project icon

Give a hands up to [Marc Silvestre from pngtree.com](https://pngtree.com/freepng/flat-bowl-of-fruit_99717.html).
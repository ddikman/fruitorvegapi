using System;
using System.Linq;
using fruitorvegapi.Repository;
using Xunit;

namespace fruitorvegapi.tests.Repository
{
    public class AssetEdiblesTests
    {
        [Fact]
        public void CanInitialize()
        {
            var repository = new AssetEdibles();
            Assert.True(repository.Any());
        }
    }
}


using System;
using System.Collections.Generic;
using System.Linq;
using fruitorvegapi.Models;
using fruitorvegapi.Repository;
using Microsoft.AspNetCore.Mvc;

namespace fruitorvegapi.Controllers
{
    [Route("api/[controller]")]
    public class EdiblesController : Controller {

        private readonly IEdibles _edibles;

        public EdiblesController(IEdibles edibles) {
            _edibles = edibles;
        }

        /// <summary>
        /// Get all the fruits and vegetables.
        /// </summary>
        /// <returns>List of fruits and vegetables.</returns>
        [HttpGet]
        public IEnumerable<Edible> list() {
            return _edibles;
        }

        /// <summary>
        /// Get the details for a single fruit or vegetable.
        /// </summary>
        /// <param name="name">The name (case insensitive) to check</param>
        /// <returns>The full edible information.</returns>
        /// <response code="404">If no fruit or vegetable with a matching name could be found.</response>
        [HttpGet("{name}")]
        [Produces(typeof(Edible))]
        [ProducesResponseType(404)]
        public ActionResult get(string name) {
            Edible edible = _edibles.FirstOrDefault(e => string.Equals(e.Name, name, StringComparison.InvariantCultureIgnoreCase));
            if (edible == null) {
                return NotFound();
            }
            return Ok(edible);
        }

        /// <summary>
        /// Descriptive name for the 'kind' and 'considered' properties of the edibles.
        /// </summary>
        /// <returns>A list of how the kind enumerations map to integers.</returns>
        [HttpGet("kinds")]
        public IEnumerable<KindDescriptor> getKinds() {
            return new KindDescriptor[] {
                new KindDescriptor(EdibleKind.Fruit, "Fruit"),
                new KindDescriptor(EdibleKind.Vegetable, "Vegetable"),
                new KindDescriptor(EdibleKind.Berry, "Berry")
            };
        }
    }
}
namespace fruitorvegapi.Models
{
    public class Edible
    {
        public Edible(string name) {
            Name = name;
        }

        public Edible(string name, EdibleKind kind) : this(name, kind, kind) {
        }

        public Edible(string name, EdibleKind kind, EdibleKind considered) {
            Name = name;
            Kind = kind;
            Considered = considered;
        }

        /// <summary>
        /// Fruit or vegetable name
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// If it is a fruit or a vegetable, scientifically speaking.
        /// </summary>
        public EdibleKind Kind { get; }

        /// <summary>
        /// If it is commonly thought of as a fruit or a vegetable.
        /// </summary>
        /// <value></value>
        public EdibleKind Considered { get; }
    }
}
namespace fruitorvegapi.Models
{
    public class KindDescriptor
    {
        public KindDescriptor(EdibleKind value, string name) {
            Value = value;
            Name = name;
        }

        /// <summary>
        /// The value the representing the name.
        /// </summary>
        public EdibleKind Value { get; }

        /// <summary>
        /// Name for the kind of edible.
        /// </summary>
        public string Name { get; }
    }
}
namespace fruitorvegapi.Models {

    public enum EdibleKind {
        Fruit = 1,
        Vegetable = 2,
        Berry = 3
    }
}
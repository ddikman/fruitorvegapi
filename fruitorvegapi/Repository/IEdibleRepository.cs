using System.Collections.Generic;
using fruitorvegapi.Models;

namespace fruitorvegapi.Repository {

    public interface IEdibles : IEnumerable<Edible> {
    }

}
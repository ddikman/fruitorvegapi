using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Json;
using System.Linq;
using System.Reflection;
using System.Text;
using fruitorvegapi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace fruitorvegapi.Repository {

    /// <summary>
    /// Keeps all fruits and vegetables known to the API.
    /// </summary>
    public class AssetEdibles : IEdibles {

        private static readonly string AssetPath = "fruitorvegapi.Assets.edibles.json";
        private readonly List<Edible> _edibles;

        public AssetEdibles() {
            var json = LoadAsset();
            _edibles = ParseJson(json).ToList();
        }

        private string LoadAsset() {
            var assembly = typeof(AssetEdibles).GetTypeInfo().Assembly;
            Stream resource = assembly.GetManifestResourceStream(AssetPath);
            var data = new byte[resource.Length];
            resource.Read(data, 0, data.Length);
            return Encoding.UTF8.GetString(data);
        }

        private IEnumerable<Edible> ParseJson(string json) {
            var items = JArray.Parse(json);
            return items.Select(ParseEdible).ToList();
        }

        private Edible ParseEdible(dynamic value) {
            return new Edible(
                (string)value.name,
                ParseKind((string)value.kind),
                ParseKind((string)value.considered)
            );
        }

        private EdibleKind ParseKind(string kind) {
            switch(kind) {
                case "vegetable":
                    return EdibleKind.Vegetable;
                case "fruit":
                    return EdibleKind.Fruit;
                case "berry":
                    return EdibleKind.Berry;
            }

            throw new NotSupportedException("Edibles json asset is invalid, unexpected edible kind encountered: " + kind);
        }


        IEnumerator IEnumerable.GetEnumerator()
        {
            return _edibles.GetEnumerator();
        }
        public IEnumerator<Edible> GetEnumerator()
        {
            return _edibles.GetEnumerator();
        }
    }
}